/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: lora.h
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/
 
#ifndef LORA_H_
#define LORA_H_

#define DEBUG_BAUD   9600

void OnTxDone( void );    // @brief Function to be executed on Radio Tx Done event
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr ); // @brief Function to be executed on Radio Rx Done event
void OnTxTimeout( void ); // @brief Function executed on Radio Tx Timeout event
void OnRxTimeout( void ); // @brief Function executed on Radio Rx Timeout event
void OnRxError( void );   // @brief Function executed on Radio Rx Error event
void OnFhssChangeChannel( uint8_t channelIndex ); // @brief Function executed on Radio Fhss Change Channel event
void OnCadDone( void );   // @brief Function executed on CAD Done event

void lora_init( void );
void lora_runHandler( void );

void lora_run_rx( bool isMaster );
void lora_run_tx( bool isMaster );
void lora_run_rxTimeout( bool isMaster );
void lora_run_rxError( bool isMaster );

#endif

