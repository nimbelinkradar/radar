/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: sensors.h
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/

#ifndef SENSORS_H_
#define SENSORS_H_

typedef struct
{
    float latitude;
    float longitude;
} GPS_Sensor_Data_T;

typedef struct
{
    GPS_Sensor_Data_T gps;
    float temp;
    float humidity;
    float barometer;
    float battery1;
    float battery2;
} Data_T;

bool sensors_init( Data_T * const sensor_data );
bool sensors_read( Data_T * const sensor_data );
bool sensors_sendToSkywire( Data_T * const sensor_data );

#endif

