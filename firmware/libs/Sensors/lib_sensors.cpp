/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: sensors.cpp
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/

#include "mbed.h"
#include "lib_skywire.h"
#include "lib_sensors.h"
#include "LPS331.h"
#include "LIS3DH.h"
#include "LM75B.h"

/********************************************************************************
*                         L O C A L   D E F I N I T I O N S
********************************************************************************/

I2C i2c(PB_9,PB_8);

LPS331 pressure(i2c);
LM75B LM75_temp(PB_9,PB_8);
LIS3DH accel(i2c, LIS3DH_V_CHIP_ADDR, LIS3DH_DR_NR_LP_100HZ, LIS3DH_FS_2G);

/********************************************************************************
*                         L O C A L   F U N C T I O N S
********************************************************************************/

bool sensors_init( Data_T * const sensor_data )
{    
    sensor_data->temp = 0;
    
    sensor_data->barometer = 0;
    
    sensor_data->battery1 = 0;
    sensor_data->battery2 = 0;
    
    sensor_data->gps.latitude = 0;
    sensor_data->gps.longitude = 0;

//#if SKYWIRE == 0
    LM75_temp.open();
//#endif

    return 0;
}

bool sensors_read( Data_T * const sensor_data )
{
    sensor_data->temp = (float)LM75_temp;
    sensor_data->barometer = (float)pressure.value() / 4096;
        
    return 0;
}

/*bool sensors_sendToEeprom( Data_T * const sensor_data )
{
    bool success = false;
    
    success = EEPROM_writeData(sensor_data);
    
    return success;
}*/

bool sensors_sendToSkywire( Data_T * const sensor_data )
{
    pc_tx(sensor_data);
    skywire_tx(sensor_data);
    wait(10);
    
    return 0;
}

/*bool sensors_sendToLoRa( Data_T * const sensor_data )
{
    //
    
    return 0;
}

bool sensors_readFromLoRa( Data_T * const sensor_data )
{
    //
    
    return 0;
}*/
