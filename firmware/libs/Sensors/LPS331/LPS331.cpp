//**********************
// LPS331.cpp for mbed
//
// LPS331 barometer(P0_5,P0_4);
// or
// I2C i2c(P0_5,P0_4);
// LPS331 barometer(i2c);
//
// (C)Copyright 2014 All rights reserved by Y.Onodera
// http://einstlab.web.fc2.com
//**********************

#include "mbed.h"
#include "LPS331.h"

LPS331::LPS331 (PinName sda, PinName scl) : _i2c(sda, scl) {
    init();
}
LPS331::LPS331 (I2C& p_i2c) : _i2c(p_i2c) {
    init();
}

void LPS331::put(unsigned char a, unsigned char b)
{
    buf[0]=a;
    buf[1]=b;
    _i2c.write(LPS331_ADDR, buf, 2);
}


void LPS331::get(unsigned char a)
{
    buf[0] = a;
    _i2c.write(LPS331_ADDR, buf, 1, true); // no stop, repeated
    _i2c.read( LPS331_ADDR, buf, 1);

}

long LPS331::value()
{

    // XL first and H last
    // get press_xl
    get(LPS331_PRESS_POUT_XL_REH);
    press.byte.LB=buf[0];
    // get tpress_low
    get(LPS331_PRESS_OUT_L);
    press.byte.HB=buf[0];
    // get press_high
    get(LPS331_PRESS_OUT_H);
    press.byte.UB=buf[0];
    return press.Val;
 
    // hPa = press.Val / 4096
    // Pa = press.Val / 40.96   
}


short LPS331::temperature()
{

    // L first and H last
    // get tpress_low
    get(LPS331_TEMP_OUT_L);
    temp.byte.LB=buf[0];
    // get press_high
    get(LPS331_TEMP_OUT_H);
    temp.byte.HB=buf[0];
    return temp.S;
 
    // C = 42.5 + temp.S / 480
    // range:0 to 80C
    // accuracy:+-2C
}


void LPS331::init()
{
    // Power ON Cycle=1Hz
    put(LPS331_CTRL_REG1, 0x90);
}



