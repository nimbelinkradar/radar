/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: skywire.h
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/

#ifndef SKYWIRE_H_
#define SKYWIRE_H_

#define SKYWIRE_BAUD 115200
#define DEBUG_BAUD   9600

#include "mbed.h"
#include "lib_sensors.h"

#define DeviceID "NimbeLink_LoRa"  //Freeboard DweetIO unique ID

void read_line();
int WaitForResponse(char* response, int num);

void skywire_rx_interrupt();
void skywire_queryGps(Data_T * const sensor_data);
void skywire_sendSMS( void );
void skywire_init( void );
void skywire_tx(Data_T * const sensor_data);
void pc_tx(Data_T * const sensor_data);

#endif

