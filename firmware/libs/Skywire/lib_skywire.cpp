/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: skywire.cpp
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/

#include "lib_skywire.h"

/********************************************************************************
*                         L O C A L   D E F I N I T I O N S
********************************************************************************/

DigitalOut status_led(LED1);
DigitalOut skywire_en(SKY_ONOFF);
DigitalOut skywire_rts(MCU_RTS);

Serial skywire(MCU_TX, MCU_RX);
Serial debug3(USART6_TX, USART6_RX);

char msg[3];
char str[255];

volatile int rx_in=0;
volatile int rx_out=0;
const int buffer_size = 255;

char rx_buffer[buffer_size+1];
char rx_line[buffer_size];

/********************************************************************************
*                         L O C A L   F U N C T I O N S
********************************************************************************/

void read_line()
{
    int i;
    i = 0;
    // Start Critical Section - don't interrupt while changing global buffer variables
    __disable_irq();
    // Loop reading rx buffer characters until end of line character
    
    while ((i==0) || (rx_line[i-1] != '\n'))
    {
        // Wait if buffer empty
        if (rx_in == rx_out)
        {
            // End Critical Section - need to allow rx interrupt to get new characters for buffer
            __enable_irq();
            while (rx_in == rx_out)
            {
                // SOMETHING
            }
            // Start Critical Section - don't interrupt while changing global buffer variables
            __disable_irq();
        }
        rx_line[i] = rx_buffer[rx_out];
        i++;
        rx_out = (rx_out + 1) % buffer_size;
    }
    // End Critical Section
    __enable_irq();
    rx_line[i-1] = 0;
    return;
}

int WaitForResponse(char* response, int num)
{
    do
    {
        read_line();
        debug3.printf("Waiting for: %s, Recieved: %s\r\n", response, rx_line);
    } while (strncmp(rx_line, response, num));
    return 0;
}

/********************************************************************************
*                         G L O B A L   F U N C T I O N S
********************************************************************************/

void skywire_init( void )
{
    debug3.baud(DEBUG_BAUD);
    skywire.baud(SKYWIRE_BAUD);
    
    debug3.printf("SystemCoreClock = %d Hz\r\n", SystemCoreClock);

    skywire.attach(&skywire_rx_interrupt, Serial::RxIrq);
    skywire_rts=0;
    status_led=0;
    debug3.printf("Waiting for Skywire to Boot...\r\n");
    
    //Enable Skywire
    skywire_en=0;
    wait(2);
    skywire_en=1;
    wait(2);
    skywire_en=0;

    status_led=1;
    wait(5);

    //Turn off echo
    skywire.printf("ATE0\r\n");
    WaitForResponse("OK", 2);

    debug3.printf("Connecting to Network...\r\n");
    // get IP address
    skywire.printf("AT#SGACT=1,1\r\n");
    WaitForResponse("#SGACT", 6);
    WaitForResponse("OK", 2);

    // connect to dweet.io
    skywire.printf("AT#HTTPCFG=1,\"dweet.io\",80,0\r\n");
    WaitForResponse("OK", 2);
    
    // send SMS
    //skywire_sendSMS();
}

void skywire_rx_interrupt()
{
    // Loop just in case more than one character is in UART's receive FIFO buffer
    // Stop if buffer full
    while ((skywire.readable()) && (((rx_in + 1) % buffer_size) != rx_out))
    {
        rx_buffer[rx_in] = skywire.getc();
        rx_in = (rx_in + 1) % buffer_size;
    }
    return;
}

/* There seems to be an issue with calling this function. Doesn't work. */
void skywire_sendSMS( void )
{
    skywire.printf("AT+CMGF=1\r\n");
    WaitForResponse("OK", 2);
    
    skywire.printf("AT+CMGS=\"+19522218892\"\r\n");
    WaitForResponse(">", 2);
    
    skywire.printf("Hello world!\032");
    WaitForResponse("+CMGS: xx", 2);
}

void skywire_queryGps(Data_T * const sensor_data)
{
    float latitude; //TODO: Remove - Emal
    float longitude; //TODO: Remove - Emal
    int number;
    
    //get location approximation from cell tower information
    skywire.printf("AT#AGPSSND\r\n");
    WaitForResponse("#AGPSRING:", 10);

    //debug3.printf("Skywire says: %s\r\n", rx_line);
    sscanf(rx_line, "%s %d,%f,%f,", str, &number, &latitude, &longitude);
    debug3.printf("Location: Latt:%f, Long:%f\r\n", latitude, longitude);
    wait(3);
    
    sensor_data->gps.latitude = latitude;
    sensor_data->gps.longitude = longitude;
}

void skywire_tx(Data_T * const sensor_data)
{
    static uint16_t counter = 0;
    
    /*Fake Battery Voltage for now: TODO Emal - Remove after Demo*/
    static double volt = 6.2;
    sensor_data->battery1 = volt + 0.1;
    sensor_data->battery2 = volt;
    if(volt > 6.3)
        volt = 6.0;
    else
        volt += 0.1;
    /*Fake Battery Voltage for now*/
    
    //get location approximation from cell tower information
    //skywire_queryGps(sensor_data);
    
    //Report Sensor Data to dweet.io
    skywire.printf("AT#HTTPQRY=1,0,\"/dweet/for/%s"
                   "?ticker=%f"
                   "&temp=%.3f"
                   "&press=%.3f"
                   "&humid=%.3f"
                   "&volt1=%.3f"
                   "&volt2=%.3f"
                   "&lat=%f"
                   "&long=%f"
                   "\"\r\n",
                   DeviceID,
                   (float)counter,
                   sensor_data->temp,
                   sensor_data->barometer,
                   sensor_data->humidity,
                   sensor_data->battery1,
                   sensor_data->battery2,
                   sensor_data->gps.latitude,
                   sensor_data->gps.longitude);
    WaitForResponse("#HTTPRING", 9);
    
    skywire.printf("AT#HTTPRCV=1\r\n");
    WaitForResponse("OK", 2);
    
    wait(5);
    counter++;
}

void pc_tx(Data_T * const sensor_data)
{
    //Report Sensor Data to PC
    debug3.printf("Temp = %.3f\r\n", sensor_data->temp);
    debug3.printf("Pressure = %.3f\r\n", sensor_data->barometer);
    //debug3.printf("Accel = %.3f, %.3f, %.3f\r\n", sensor_data->axis[0], sensor_data->axis[1], sensor_data->axis[2]);   
}

