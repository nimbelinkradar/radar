/*********************************************************************************
 * Copyright (c) 2015 Emal Alwis, Stephen Iversen, Colin James,                  *
 *                    Ta Tan Lam,  John Thole, Ethan Yanna                       *
 *                                                                               *
 * Permission is hereby granted, free of charge, to any person obtaining a copy  *
 * of this software and associated documentation files (the "Software"), to deal *
 * in the Software without restriction, including without limitation the rights  *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     *
 * copies of the Software, and to permit persons to whom the Software is         *
 * furnished to do so, subject to the following conditions:                      *
 *                                                                               *
 * The above copyright notice and this permission notice shall be included in    *
 * all copies or substantial portions of the Software.                           *
 *                                                                               *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     *
 * THE SOFTWARE.                                                                 *
 *********************************************************************************
 * Description: main.cpp
 *
 * Main file for
 *
 * Created on: Feb 17, 2015
 *     Author: Emal Alwis <alwis002@umn.edu>
 *******************************************************************************/

#include "mbed.h"
#include "Drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h" // Device header
#include "lib_skywire.h"
#include "lib_lora.h"
#include "lib_sensors.h"

/********************************************************************************
*                         L O C A L   D E F I N I T I O N S
********************************************************************************/

/* Set this flag to '0' for the Sensor Unit (Portable) or to '1' for the Cell Unit (Stationary) */
#define SKYWIRE_ENABLED 1
#define SKYWIRE_RATE 6000 /* 5 min = 6000 */
#define LCD_BAUD   9600

InterruptIn button_1(Ext_Btn1); // Not Working
InterruptIn button_2_in(Ext_Btn2);
InterruptIn button_2_out(Ext_Btn2);
InterruptIn button_3(Ext_Btn3);
InterruptIn button_4(Ext_Btn4);

DigitalOut heartBeat(HrtBt);
DigitalOut mux_S0(MUX_S0);
DigitalOut mux_S1(MUX_S1);
DigitalOut skywirePower(Sky_Pwr_En);

Data_T Sensor_Data;
Serial debug1(USART6_TX, USART6_RX);
Serial lcd(USART2_TX, USART2_RX);

/********************************************************************************
*                      I N T E R R U P T   F U N C T I O N S
********************************************************************************/

void intrpt_button_1() /* Not Working */
{
    debug1.printf("BUTTON 1 PRESSED!\r\n");
}

void intrpt_button_2_out()
{
    debug1.printf("BUTTON 2 PRESSED OUT!\r\n");
}

void intrpt_button_2_in()
{
    debug1.printf("BUTTON 2 PRESSED IN!\r\n");
}

void intrpt_button_3()
{
    debug1.printf("BUTTON 3 PRESSED!\r\n");
}

void intrpt_button_4()
{
    debug1.printf("BUTTON 4 PRESSED!\r\n");
}

/********************************************************************************
*                        L O C A L   F U N C T I O N S
********************************************************************************/

void board_init( Data_T * const sensor_data )
{
    // Configure Serial
    lcd.baud(LCD_BAUD);
    debug1.baud(DEBUG_BAUD);

    lcd.printf("\nRESET\r\n\n");
    debug1.printf("\nRESET\r\n\n");
    debug1.printf("Board Initialization...\r\n");

    /* Set heartBeat LED High */
    heartBeat = 1;

    /* Configure Multiplexor for LCD */
    mux_S0 = 0;
    mux_S1 = 0;

    // Use external pullup for pushbutton
    button_1.mode(PullNone); /* Not Working */
    button_2_in.mode(PullNone);
    button_2_out.mode(PullNone);
    button_3.mode(PullNone);
    button_4.mode(PullNone);

    // Delay for initial pullup to take effect
    wait(1);

    // Attach the address of the interrupt handler routine for pushbutton
    button_1.fall(&intrpt_button_1);  // attach the address of the intrpt_button_1 function to the falling edge
    button_2_in.fall(&intrpt_button_2_in);  // attach the address of the intrpt_button_2 function to the falling edge
    button_2_out.rise(&intrpt_button_2_out);  // attach the address of the intrpt_button_2 function to the falling edge
    button_3.fall(&intrpt_button_3);  // attach the address of the intrpt_button_3 function to the falling edge
    button_4.fall(&intrpt_button_4);  // attach the address of the intrpt_button_4 function to the falling edge

    // Configure Sensors
    debug1.printf("Sensors Initialization...\r\n");
    sensors_init(sensor_data);

    // Configure LoRa
    debug1.printf("LoRa Initialization...\r\n");
    lora_init();
}

/********************************************************************************
*                                   M A I N
********************************************************************************/
int main( void )
{
    uint16_t delay = SKYWIRE_RATE;
    Data_T * const sensor_data = &Sensor_Data;
    
    // Main Init
    board_init(sensor_data);

    // Main Loop
    while(1)
    {
        heartBeat = !heartBeat;
        wait(.1);

        /* Skywire Handler */
        switch(SKYWIRE_ENABLED)
        {
            case 1: // Skywire Router
                if(delay > SKYWIRE_RATE)
                {
                    debug1.printf("Starting Skywire...\r\n");

                    /* Set 4V Rail High */
                    skywirePower = 1;
                    wait(1);

                    skywire_init();
                    skywire_queryGps(sensor_data);

                    // TODO: Emal: POLLING SENSORS FOR DEBUGGING ONLY, remove later
                    debug1.printf("Reading Sensors...\r\n");
                    sensors_read(sensor_data);

                    // Run Skywire
                    pc_tx(sensor_data);
                    skywire_tx(sensor_data);
                    wait(2);
                    
                    debug1.printf("Shutting Down Skywire...\r\n");
                    skywirePower = 0;
                    delay = 0;
                }
                else
                {
                    delay++;
                }
                break;

            case 0: // Sensor Node
                // Poll Sensors
                debug1.printf("Reading Sensors...\r\n");
                sensors_read(sensor_data);
                break;

            default:
                break;
        }

        // Run LoRa
        //debug1.printf("Running LoRa...\r\n");
        lora_runHandler();
    }
}

